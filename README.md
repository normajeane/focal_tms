# Requirements
- `numpy` and `matplotlib`

# Plot B-field 

Contour plots of the Bx, By, and Bz components of the magnetic field along a certain plane.

## Basic Overview

- [Biot-Savart Magnetic Field Calculator](https://github.com/vuthalab/biot-savart)

Given an input coil, the tool can both calculate, and plot the magnetic field vector over a finite discrete volume of space around the coil.
A target volume is produced as a series of points within a rectangular prism bounding box around the coil. 
The magnetic field at each point in the target volume is obtained by numerically integrating using the **Biot-Savart law**, resulting in a 4D array of size (X, Y, Z, 3) as the output.


# Example
- Figure-Eight Coil (Transcranial Magnetic Stimulation)

![](./figure/fig_TMS.png)
![](./figure/fig_Bfield.png)


# Others

- Lenz law: 닫힌 회로에 유입되는 자기 선속이 변할 때, 그 선속의 변화를 방해하는 자기장을 형성한다는 법칙.
- Faraday law: 닫힌 회로를 통과하는 자기 선속의 변화량은 곧 회로의 유도 기전력과 같다는 법칙.
- Biot-Savart law: 전류가 흐르는 도선에 의해 생성되는 미소 자기장을 모델링 함.

 $\mathbf{B}(\mathbf{r}) = \frac{\mu_0}{4\pi} \int_C \frac{I d\mathbf l\times\mathbf{r'}}{|\mathbf{r'}|^3}$

![](./figure/fig_eddy.png)[^3]

- Lorentz force: 전하량을 가진 물체가 전/자기장 내에서 받는 힘
- Figure-8 coil에서 radius가 클수록 세기가 세지고 초점이 넓어짐. (Tradeoff: depth–focality [^1])
- Neuronal activation by TMS[^2]

![](./figure/fig_neuro.png)

[^1]: Deng et al. *Brain Stimul.* 2013
[^2]: MJ Edewards et al. *Lancet Neurol.* 2008
[^3]: T Kato et al. *J. Appl. Phys.* 2012
