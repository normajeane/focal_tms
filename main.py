'''
Magnetic field of the Figure-Eight Coil (Transcranial Magnetic Stimulation)

Hyunsoo Shin
2022.11.29
'''
import os
from utils import BiotSavart as bs

# n segments each, with radius of 5 cm
# spaced out by 2 cm, located at z = +/- 1 respectively
# 1 amp of current

offset = [-5, 5] # Y-axis
num_segments = [100, 100]
radius = [2.5, 2.5]
spacing = [2, 2]
current = [1000, -1000]
vol_resol = 0.5
coil_resol = 0.5
height_of_plane = 1
box_size = (20, 20, 5)
start_point = (-10, -10, 10)

if not os.path.exists('outputs'):
    os.makedirs('outputs')

bs.tms_coil('outputs/tms_l.txt', offset[0], num_segments[0], radius[0], spacing[0], current[0])
bs.tms_coil('outputs/tms_r.txt', offset[1], num_segments[1], radius[1], spacing[1], current[1])
#bs.plot_coil('tms1.txt', 'tms2.txt')


start_point_l = start_point
start_point_r = start_point
start_point_total = start_point

bs.write_target_volume("outputs/tms_l.txt", "outputs/targetvol_l", box_size, start_point_l, coil_resolution=coil_resol, volume_resolution=vol_resol)
bs.write_target_volume("outputs/tms_r.txt", "outputs/targetvol_r", box_size, start_point_r, coil_resolution=coil_resol, volume_resolution=vol_resol)

# produce the target volumes we want
h_l = bs.read_target_volume("outputs/targetvol_l")
h_r = bs.read_target_volume("outputs/targetvol_r")

# use linear superposition of magnetic fields, to get the combined effects of multiple coils
h_total = h_l + h_r


bs.plot_fields(h_total, box_size, start_point_total, vol_resol, which_plane='z', level=height_of_plane, num_contours=50)
